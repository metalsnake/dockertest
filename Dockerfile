
version: '2.2'
services:
   server-1:
      image: consul:1.5.2
      command: consul agent -server -bootstrap-expect=3 -data-dir /tmp/consul -node=server-1
      restart: always

   server-2:
      image: consul:1.5.2
      command: consul agent -server -bootstrap-expect=3 -data-dir /tmp/consul -retry-join=server-1 -node=server-2
      restart: always

   server-3:
      image: consul:1.5.2
      command: consul agent -server -bootstrap-expect=3 -data-dir /tmp/consul -retry-join=server-1 -node=server-3
      restart: always

   consul-ui:
      image: consul:1.5.2
      container_name: consul-cont
      command: consul agent -data-dir /tmp/consul -retry-join=server-1 -client 0.0.0.0 -ui -node=client-ui
      restart: always
      ports:
        - 8500:8500
